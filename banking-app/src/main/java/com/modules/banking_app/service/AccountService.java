package com.modules.banking_app.service;

import java.util.List;

import com.modules.banking_app.dto.AccountDto;
import com.modules.banking_app.entity.Account;

public interface AccountService {

	AccountDto createAccount(AccountDto accountdto);
	
	AccountDto getAccountById(Long id);
	
	AccountDto deposit(Long id,double amount);
	
	AccountDto withdraw(Long id, double amount);
	
	List<AccountDto> getAllAccounts();
	
	void deleteAccount(Long id);
}
