package com.modules.banking_app.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.modules.banking_app.dto.AccountDto;
import com.modules.banking_app.entity.Account;
import com.modules.banking_app.mapper.AccountMapper;
import com.modules.banking_app.repository.AccountRepo;

@Service
public class AccountServiceImpl implements AccountService{
	
	@Autowired
	private AccountRepo accountrepo;

	
	public AccountServiceImpl(AccountRepo accountrepo) {
		
		this.accountrepo = accountrepo;
	}


	@Override
	public AccountDto createAccount(AccountDto accountdto) {
		Account account=AccountMapper.mapToAccount(accountdto);
		Account savedAccount=accountrepo.save(account);
		return AccountMapper.mapToAccountDto(savedAccount);
	}


	@Override
	public AccountDto getAccountById(Long id) {
		Account account=accountrepo
				.findById(id)
				.orElseThrow(()-> new RuntimeException("Account does not exist"));
		return AccountMapper.mapToAccountDto(account);
	}


	@Override
	public AccountDto deposit(Long id, double amount) {
		Account account=accountrepo
				.findById(id)
				.orElseThrow(()-> new RuntimeException("Account does not exist"));
		double total=account.getBalance()+ amount;
		account.setBalance(total);
		Account savedAccount=accountrepo.save(account);
		
		return AccountMapper.mapToAccountDto(savedAccount);
	}


	@Override
	public AccountDto withdraw(Long id, double amount) {
		Account account=accountrepo
				.findById(id)
				.orElseThrow(()-> new RuntimeException("Account does not exist"));
	
		
		if(account.getBalance()<amount) {
			throw new RuntimeException("insufficient balance");
		}
		
		double total=account.getBalance()-amount;
		account.setBalance(total);
		Account savedAccount=accountrepo.save(account);
		
		return AccountMapper.mapToAccountDto(savedAccount);
	}


	@Override
	public List<AccountDto> getAllAccounts() {
		List<Account>accounts=accountrepo.findAll();
		return accounts.stream().map((account) -> AccountMapper.mapToAccountDto(account))
				.collect(Collectors.toList());
		
	}


	@Override
	public void deleteAccount(Long id) {
		Account account=accountrepo
				.findById(id)
				.orElseThrow(()-> new RuntimeException("Account does not exist"));
		accountrepo.deleteById(id);
		
	}

}
