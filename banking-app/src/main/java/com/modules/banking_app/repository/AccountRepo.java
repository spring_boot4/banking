package com.modules.banking_app.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.modules.banking_app.entity.Account;

public interface AccountRepo  extends JpaRepository<Account,Long>{

}
